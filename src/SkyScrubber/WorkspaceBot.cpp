#include "WorkspaceBot.h"

using namespace SKY;
using namespace Motor;

WorkspaceBot::WorkspaceBot(int width, int height)
    : m_Extent(0, 0, width, height)
    , m_EffectorPos( m_Extent.GetCenter() )
    , m_PositionController(*this)
{
}

void WorkspaceBot::Init()
{
    RobotInstance::Init();
    RC->Log("WorkspaceBot Init");

    InitializeMotors();
    InitializeControllers();
}    
     
void WorkspaceBot::Update()
{    
    RobotInstance::Update();

    // Let our controllers perform their corrections
    m_EffectorPos = m_PositionController.Evaluate();
}    
     
void WorkspaceBot::Shutdown()
{
    RobotInstance::Shutdown();
}

void WorkspaceBot::MoveTo(const Geom2D::Point& toPoint)
{
    // Probably not intentional, let's assert to warn ourselves of faulty implementation
    check(m_Extent.Contains(toPoint));

    // Manipulate local copy to preserve const-correctness please.
    Geom2D::Point clampedPoint = toPoint;
    m_Extent.ClampPoint(clampedPoint);

    m_PositionController.SetPositionReference(clampedPoint);
}
#ifdef SS_DEBUG
Motor::Stepper& SKY::WorkspaceBot::Debug_AttachStepper(short steps, short dir_pin, short step_pin, short enable_pin)
{
    return RC->AttachMotor<Stepper>(steps, dir_pin, step_pin, enable_pin);
}
#endif // SS_DEBUG

void WorkspaceBot::InitializeMotors()
{
    // Order is actually important here. These must be initialized in correspondance with the ECorner enum
    // Alternatively, we could store a pointer to these motors at this step and directly access them afterwards
    const short steps = (360/1.8);
    Stepper& motor = RC->AttachMotor<Stepper>(steps, 7, 6, 5);
    motor.SetMicroStep(EMicroStep::MS_1);
    motor.SetRPM(60);
    motor.SetEnabled(true);
}

void WorkspaceBot::InitializeControllers()
{
}

Stepper* WorkspaceBot::GetMotor(ECorner atCorner) const
{
    check(RC);
    return (Stepper*)RC->GetMotor(atCorner);
}

void WorkspaceBot::OnStepperValueReached(Motor::Stepper* stepper, long stepCount)
{
}

void WorkspaceBot::OnStepperOverCurrent(Motor::Stepper* stepper, float currentVal)
{
}

void WorkspaceBot::BindMotorEvents(Stepper& stepper)
{
    // Finally... this actually works...
    stepper.SetMotorStepReachedCallback([](Stepper* stepper, long stepCount) 
    {
        WorkspaceBot* self = (WorkspaceBot*)(RobotInstance::GetInstance());
        self->OnStepperValueReached(stepper, stepCount);
    });

    stepper.SetMotorCurrentCallback([](Stepper* stepper, float currentVal)
    {
    });
}

#pragma once
#ifndef I_CONTROLLER_H
#define I_CONTROLLER_H

#include "../../Framework/Math/Geom2D.h"
#include "../../Framework/Math/Vector.h"

namespace SKY
{
    /* A discrete time controller in a unity feedback architecture
    *  --*--[C]-----[P]--*--
    *    |               |
    *    *---------------*
    */
    class IController
    {
    public:
        virtual void Evaluate() = 0;
        virtual ~IController() {}
    protected:
        /* Returns the final controller output signal, u, typically via a difference equation */
        virtual float ComputeControlSignal() = 0;
    private:

    };
}
#endif // !I_CONTROLLER_H

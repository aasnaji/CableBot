#include "PositionController.h"
#include "../WorkspaceBot.h"

namespace SKY
{
    PositionController::PositionController(WorkspaceBot& botOwner)
        : m_BotOwner(botOwner)
    {
    }

    Geom2D::Point PositionController::Evaluate()
    {
        Geom2D::Point pos = m_BotOwner.GetEffectorPos();

        if(pos == m_Reference)
            return m_Reference;

        // process error
        Motor::Stepper* motor = m_BotOwner.GetMotor(ECorner::LOWER_LEFT);
        if(!motor)
            return m_Reference;

        // This is effectively where we are in the X-direction
        float motorDist = motor->GetStepsPerDistance() * motor->GetAccumulatedStep();

        // Find delta x that needs to be applied
        float dx = (m_Reference.X - motorDist);

        // Order the motor to move by that delta to arrive at our reference. In this example, we use blocking mode
        motor->MoveDist(dx, false);

        // Feedback to workspace so it updates its current position
        return Geom2D::Point(motorDist, 0);
    }
}
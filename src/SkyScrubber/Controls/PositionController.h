#pragma once
#ifndef POSITION_CONTROLLER_H
#define POSITION_CONTROLLER_H
#include "IController.h"

namespace SKY
{
    class WorkspaceBot;

    class PositionController
    {
    public:
        PositionController(WorkspaceBot& botOwner);

        /* IController Interface */
        Geom2D::Point Evaluate();

        /* Set a reference input for the controller to track */
        void SetPositionReference(Geom2D::Point pos) { m_Reference = pos; }
    protected:

    private:
        Geom2D::Point m_Reference;

        WorkspaceBot& m_BotOwner;
    };
}
#endif // !POSITION_CONTROLLER_H

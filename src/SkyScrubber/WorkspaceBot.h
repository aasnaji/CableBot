#pragma once
#include "../HAL/HALFwd.h"
#include "../Framework/Math/Geom2D.h"

#include "Controls/PositionController.h"

namespace SKY
{
    enum ECorner : short
    {
        UPPER_LEFT = 0,
        UPPER_RIGHT,
        LOWER_LEFT,
        LOWER_RIGHT
    };

    class WorkspaceBot : public RobotInstance
    {
    public:
        WorkspaceBot(int width, int height);

        virtual void Init() override;
        virtual void Update() override;
        virtual void Shutdown() override;

        void MoveTo(const Geom2D::Point& toPoint);

        Motor::Stepper* GetMotor(ECorner atCorner) const;

        inline Geom2D::Rectangle GetExtent() const      { return m_Extent; }
        inline Geom2D::Point     GetEffectorPos() const { return m_EffectorPos; }
        
#ifdef SS_DEBUG
        Motor::Stepper& Debug_AttachStepper(short steps, short dir_pin, short step_pin, short enable_pin = -1);
#endif // SS_DEBUG

    protected:
        void InitializeMotors();
        void InitializeControllers();

        void OnStepperValueReached(Motor::Stepper* stepper, long stepCount);
        void OnStepperOverCurrent(Motor::Stepper* stepper, float currentVal);

        void BindMotorEvents(Motor::Stepper& stepper);
    private:
        Geom2D::Rectangle m_Extent;
        Geom2D::Point m_EffectorPos;

        /* Controllers */
        PositionController m_PositionController;
    };
}
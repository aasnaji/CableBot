#pragma once
#ifndef DELEGATE_H
#define DELEGATE_H

#if 0
template<typename T>
class Delegate;

template<typename Ret, typename ...Args>
class Delegate<Ret(Args...)>
{
    using CallbackType = Ret(*)(void*, Args...);

public:
    // Invoke the function. Alternatively, can define Broadcast method.
    Ret operator()(Args...args)
    {
        return mCallback(mCaller, args...);
    }

    bool operator==(const Delegate& Other)
    {
        return mCaller == Other.mCaller && mCallback == Other.mCallback;
    }

    // Factory Method
    template<Ret(*FuncPtr)(Args...)>
    static Delegate Create()
    {
        return Delegate(nullptr, &GlobalCaller<FuncPtr>);
    }
private:

    void* mCaller = nullptr;
    CallbackType mCallback = nullptr;

    Delegate(void* InCaller, CallbackType InCallback)
    : mCallback(InCallback)
    , mCaller(InCaller)
    {
    }

    template<Ret(*FuncPtr)(Args...)>
    static Ret GlobalCaller(void*, Args...arg)
    {
        return FuncPtr(args...);
    }
};
#endif

#if 1
struct DelegateBase
{
    virtual void DoInvoke() = 0;
    virtual bool IsBound() = 0;
};

/*  NOTE:
*   I do not want to support member function pointers yet. I suggest we defer to static functions when dealing
*   with events. Member function invocations will require countless template instantiation, and our program will 
*   quickly grow in size. Moreover, our compilation speeds will tank. Let's leave a proper event systems to a high
*   level application instead..
*/
struct MemberFuncInvocation
{
};

template<typename InClass>
struct TMemberFuncInvocation : public MemberFuncInvocation
{
    InClass InObject;
};

#define MAKE_PARAM_NAME(ParamType) (ParamTypeParam##__COUNTER__)    // Example: (int) -> intParam_1
#define MAKE_FUNC_PARAM(ParamType, ParamName) (ParamType ParamName) // Example: int intParam_1

// Might need an indirection... create a function invoker that generates its own param types and names. Delegate macro simply calls it?
#define CONCAT_PARAM_NAMES()
#define DECLARE_DELEGATE(Name, ReturnType)                                                  MAKE_DELEGATE(Name, ReturnType, , ,)
#define DECLARE_DELEGATE_OneParam(Name, ReturnType, ParamType1)                             

/* Name: Name of the Delegate struct
*  ReturnType: Return type of functions bound to this delegate
*  ParamTypes: The type of parameters this delegation function signature uses
*  ParamNames: Proxy object that contains the parameter names to pass to function invokation
*  ArgumentList: Proxy Object that is the ParamType followed by its ParamName. Passed to function declarations so that we can access the arguments
*/
#define MAKE_DELEGATE(Name, ReturnType, ParamTypes, ParamNames, ArgumentList) \
struct Name : public DelegateBase
{
private:
    typedef ReturnType(*StaticFuncPtr)(ArgList);

public:
    void BindFunction(StaticFuncPtr func);
    
    /* Not Supported. See above note*/
    template<typename InClass>
    void BindFunction(InClass InObject, ReturnType(InClass::*Func)(ParamTypes))
    {

    }

    ReturnType Broadcast(ArgumentList)
    {
        // func(ArgumentList)
    }

private:
    StaticFuncPtr mStaticFunc;
};
#define DECLARE_DELEGATE(Name, ReturnType)
#endif

#endif // !DELEGATE_H

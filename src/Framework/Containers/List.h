#ifndef LIST_H
#define LIST_H

#include <stddef.h>

/*  List container recycled from our Project Everest MTE 380 Robot.
*   It was not thoroughly tested, so be cautious using it
*/
namespace
{
    template <typename DataType>
    struct FNode
    {
        DataType Data;
        FNode* Next;
        FNode* Prev;

        FNode(DataType _data, FNode* _next, FNode* _prev)
            : Data(_data)
            , Next(_next)
            , Prev(_prev)
        {
        }

    };

    template <typename DataType>
    struct TListIterator
    {
        TListIterator(FNode<DataType>* _Element)
            : Element(_Element)
        {
        }

        FNode<DataType>* Element;

        bool operator == (const TListIterator& Other)
        {
            return Element == Other.Element;
        }

        bool operator !()
        {
            return Element == nullptr;
        }

        bool operator != (const TListIterator& Other)
        {
            return Element != Other.Element;
        }

        void operator ++()
        {
            if(Element)
                Element = Element->Next;
        }

        void operator --()
        {
            if(Element)
                Element = Element->Prev;
        }

        DataType& operator *()
        {
            return Element->Data;
        }

        DataType operator ->()
        {
            return Element->Data;
        }
    };


    template <typename DataType>
    struct TList
    {
    public:
        TList();
        ~TList();
        TList(TList&& Other);

        // Insertion operation works by copying the provided data
        // Consequently, a list of pointers is able to modify the data
        // of the pointer, but never re-assign it.
        bool Insert(const DataType& _data);
        bool Contains(const DataType& _data);
        bool Remove(FNode<DataType>* _node);
        void Clear();

        // Searches the list for the specified data. Returns null if not found.
        DataType* Find(const DataType& _data);

        // Returns the number of elements in the container
        inline size_t Size() const { return m_size; }

        // This should be avoided. It only exists to allow external objects to iterate over the list
        inline TListIterator<DataType> Begin() const;
        inline TListIterator<DataType> End() const;


    private:
        FNode<DataType>* m_head;
        FNode<DataType>* m_tail;

        size_t m_size;

    };


    template <typename DataType>
    TList<DataType>::TList()
        : m_head(nullptr)
        , m_tail(nullptr)
        , m_size(0)
    {
    }

    template<typename DataType>
    TList<DataType>::~TList()
    {
        Clear();
    }

    template<typename DataType>
    TList<DataType>::TList(TList && Other)
    {
        m_head = Other.m_head;
        m_tail = Other.m_tail;
        m_size = Other.m_size;

        Other.m_head = nullptr;
        Other.m_tail = nullptr;
        Other.m_size = 0;
    }

    template<typename DataType>
    bool TList<DataType>::Insert(const DataType& _data)
    {
        if(m_size == 0)
        {
            m_head = new FNode<DataType>(_data, nullptr, m_tail);
            m_tail = m_head;
            m_size++;
            return true;
        }

        // Insert at the end of the list by creating a new node whose previous is the old tail
        FNode<DataType>* oldTail = m_tail;
        FNode<DataType>* insertion = new FNode<DataType>(_data, nullptr, oldTail);
        oldTail->Next = insertion;
        m_tail = insertion;

        m_size++;

        return true;
    }

    template<typename DataType>
    bool TList<DataType>::Contains(const DataType & _data)
    {
        if(m_size == 0)
            return false;

        FNode<DataType>* Iterator = m_head;

        while(Iterator)
        {
            if(Iterator->Data == _data)
                return true;

            Iterator = Iterator->Next;
        }

        return false;
    }

    template<typename DataType>
    bool TList<DataType>::Remove(FNode<DataType>* _node)
    {
        // Only the head remains (which is also the tail)	
        if(m_head == _node && m_size == 1)
        {
            delete m_head;
            delete m_tail;

            m_head = nullptr;
            m_tail = nullptr;

            m_size--;

            return true;
        }

        // Deleting an element off the end of the list
        if(m_tail == _node)
        {
            m_tail = m_tail->Prev;
            m_tail->Prev->Next = nullptr;

            delete _node;
            _node = nullptr;

            m_size--;

            return true;
        }

        FNode<DataType>* deleteNode = Find(_node->Data);

        // Node not found, removal failed
        if(!deleteNode)
            return false;

        if(deleteNode->Prev)
        {
            deleteNode->Prev->Next = deleteNode->Next;	// Works expectedly even if deleteNode is the last element
        }

        if(deleteNode->Next)
        {
            deleteNode->Next->Prev = deleteNode->Prev; // Works expectedly even if deleteNode is the first element
        }

        delete deleteNode;
        deleteNode = nullptr;

        m_size--;

        return true;
    }

    template<typename DataType>
    inline void TList<DataType>::Clear()
    {
        // Start popping from the end
        FNode<DataType>* Iterator = m_tail;

        while(Iterator && Iterator != m_head)
        {
            // Step backward first, and safely delete the element ahead of us
            Iterator = Iterator->Prev;

            delete Iterator->Next;
            Iterator->Next = 0;
        }

        delete m_head;
        m_head = 0;

        m_size = 0;
    }

    template<typename DataType>
    DataType* TList<DataType>::Find(const DataType & _data)
    {
        // List is empty or undefined
        if(!m_head || m_size == 0)
            return nullptr;

        FNode<DataType>* Iterator = m_head;

        // Iterate starting from the head and compare the contained data
        while(Iterator)
        {
            if(Iterator->Data == _data)
                return &Iterator->Data;

            Iterator = Iterator->Next;
        }

        return nullptr;
    }

    template<typename DataType>
    inline TListIterator<DataType> TList<DataType>::Begin() const
    {
        return TListIterator<DataType>(m_head);
    }

    template<typename DataType>
    inline TListIterator<DataType> TList<DataType>::End() const
    {
        return TListIterator<DataType>(m_tail->Next);
    }
}

#endif // !LIST_H

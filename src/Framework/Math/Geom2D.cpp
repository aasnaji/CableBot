#include "Geom2D.h"
#include "MathUtils.h"

Geom2D::Rectangle::Rectangle(int X1, int Y1, int X2, int Y2)
{
    // We could be given a top left and bottom right corner. We need to resolve these cases
    MinPoint.X = Math::Min(X1, X2);
    MinPoint.Y = Math::Min(Y1, Y2);

    MaxPoint.X = Math::Max(X1, X2);
    MaxPoint.Y = Math::Max(Y1, Y2);
}

Geom2D::Rectangle::Rectangle(const Point& P1, const Point& P2)
{
    if( !(P1.IsAbove(P2) || P1.IsRightOf(P2)) )
    {
        MinPoint = P1;
        MaxPoint = P2;
    }
    else if( (P1.IsAbove(P2) || P1.IsRightOf(P2)) )
    {
        MinPoint = P2;
        MaxPoint = P1;
    }
    else
    {
        MinPoint.X = Math::Min(P1.X, P2.X);
        MinPoint.Y = Math::Min(P1.Y, P2.Y);

        MaxPoint.X = Math::Max(P1.X, P2.X);
        MaxPoint.Y = Math::Max(P1.Y, P2.Y);
    }
}

Geom2D::Rectangle::Rectangle(const Rectangle& Other)
{
    MinPoint = Other.MinPoint;
    MaxPoint = Other.MaxPoint;
}

float Geom2D::Rectangle::Diagonal() const
{
    const float w = (float)Width();
    const float h = (float)Height();

    return Math::Sqrt(w*w + h*h);
}

bool Geom2D::Rectangle::IsValid() const
{
    return !Point::Coincident(MinPoint, MaxPoint)
         && MaxPoint.IsAbove(MinPoint)
         && MaxPoint.IsRightOf(MinPoint);
}

bool Geom2D::Rectangle::Contains(const Point& P) const
{
    return (P.X < Right()) && (P.X > Left())
    &&     (P.Y < Top()) && (P.Y > Bottom());
}

bool Geom2D::Rectangle::Contains(int X, int Y) const
{
    const Point P(X,Y);

    return Contains(P);
}

bool Geom2D::Rectangle::Overlaps(const Rectangle& Rect) const
{
    // We can easily tell when two rectangles CANNOT overlap, so simply invert that condition
    return !(MinPoint.X > Rect.MaxPoint.X || MaxPoint.X < Rect.MinPoint.X);
}

void Geom2D::Rectangle::ClampPoint(Point& P) const
{
    if(!Contains(P))
    {
        P.X = Math::Min(MaxPoint.X, Math::Max(MinPoint.X, P.X));
        P.Y = Math::Min(MaxPoint.Y, Math::Max(MinPoint.Y, P.Y));
    }
}

void Geom2D::Rectangle::Offset(int Dx, int Dy)
{
    MinPoint += Point(Dx, Dy);
}

void Geom2D::Rectangle::SetPosition(const Point& NewPos, EPivot TargetPivot)
{
    Point delta;
    switch(TargetPivot)
    {
        case Geom2D::EPivot::CENTER:
        {
            delta = NewPos - GetCenter();
        }
        break;

        case Geom2D::EPivot::BOTTOM_LEFT:
        {
            delta = NewPos - GetBottomLeft();
        }
        break;

        case Geom2D::EPivot::BOTTOM_RIGHT:
        {
            delta = NewPos - GetBottomRight();
        }
        break;

        case Geom2D::EPivot::TOP_RIGHT:
        {
            delta = NewPos - GetTopRight();
        }
        break;

        case Geom2D::EPivot::TOP_LEFT:
        {
            delta = NewPos - GetTopLeft();
        }
        break;

        default:
        break;
    }

    MinPoint += delta;
    MaxPoint += delta;
}

void Geom2D::Rectangle::SetPosition(int NewX, int NewY, EPivot TargetPivot)
{
    const Point newPos(NewX, NewY);
    SetPosition(newPos, TargetPivot);
}

void Geom2D::Rectangle::Scale(float scale, EPivot pivot)
{
}

Geom2D::Rectangle Geom2D::Rectangle::GetScaled(float scale, EPivot pivot) const
{
    return Rectangle();
}


#pragma once
#ifndef MATRIX_2D_H
#define MATRIX_2D_H

#include "Vector.h"
#include <memory>

/*  2D (hopefully affine) Transformation Matrix
*   A note about scale. I cannot imagine scenarios where I would want
*   to scale an object non-uniformly, so I assume the x and y scalings a,b are equal.
*   Similarily, rotation is not implemented since I do not forsee a scenario that would
*   warrant the calculation of cos and sin components.
*/
struct Matrix2D
{
    Matrix2D()
    {
    }

    Matrix2D(float elements[9])
    {
        m_matrix[0] = elements[0];
        m_matrix[2] = elements[2];
        m_matrix[3] = elements[3];
        m_matrix[4] = elements[4];
        m_matrix[5] = elements[5];
        m_matrix[6] = elements[6];
        m_matrix[7] = elements[7];
        m_matrix[8] = elements[8];
    }

    static Matrix2D MakeTranslation(const Vector2& vector)
    {
        
    }

    static Matrix2D MakeTranslation(float x, float y)
    {

    }

    static Matrix2D MakeScale()
    {

    }
    //static Matrix2D MakeRotation();

    inline Vector2 GetTranslation() const { return Vector2(m_matrix[6], m_matrix[7]); }
    inline float GetScale() const { return m_matrix[0]; }
    //inline Vector2 GetRotation() const;

    inline bool IsInvertible() const
    {
        // For square matrix, invertible iff det != 0
        return Determinant() != 0;
    }

    inline Matrix2D GetInverse() const
    {
        if(!IsInvertible())
        {
            return *this;
        }

        return Matrix2D();
    }

    inline float Determinant() const
    {
        // Cofactor expansion expression
        // a(ei - fh) -b(di - gf) + c(dh - eg)
        // a = 0, b = 1, ... i = 8
        return
          m_matrix[0] * (m_matrix[4]* m_matrix[8] - m_matrix[5]* m_matrix[7])
        - m_matrix[1] * (m_matrix[3]* m_matrix[8] - m_matrix[6]* m_matrix[5])
        + m_matrix[2] * (m_matrix[3]* m_matrix[7] - m_matrix[4]* m_matrix[6]);
    }

    inline Vector2 TransformVector(const Vector2& vector) const
    {
        return Vector2(
            m_matrix[0]*vector.X + m_matrix[6], // aX + x
            m_matrix[4]*vector.Y + m_matrix[7]  // aY +y
        );
    }

private:
    // An affine 2D transformation is achieved via a 3D matrix
    // where the last column is: [0 0 1]'.
    /*
    *   | 0 1 2 |        | a 0 0 |
    *   | 3 4 5 |   ==   | 0 b 0 |
    *   | 6 7 8 |        | x y 1 | 
    */
    float m_matrix[9] = {0};
};
#endif // !MATRIX_2D_H


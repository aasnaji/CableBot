#pragma once
#ifndef MATH_FWD_H
#define MATH_FWD_H

#include "Geom2D.h"
#include "Vector.h"

// Some functions to prevent cyclic inclusion and tight dependencies
namespace
{
    Geom2D::Point Vec2Point(const Vector2& v)
    {
        return Geom2D::Point(static_cast<int>(v.X), static_cast<int>(v.Y));
    }

    Vector2 Point2Vec(const Geom2D::Point& point)
    {
        return Vector2(point.X, point.Y);
    }
}
#endif // !MATH_FWD_H

#pragma once
#ifndef GEOM_2D_H
#define GEOM_2D_H

namespace Geom2D
{
    enum class EPivot
    {
        CENTER = 0,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        TOP_RIGHT,
        TOP_LEFT
    };

    struct Point
    {
        int X;
        int Y;

        Point()
        : X(0)
        , Y(0)
        {
        }

        Point(int InX, int InY)
        : X(InX)
        , Y(InY)
        {
        }

        inline Point operator-(const Point& Other) const { return Point((X - Other.X), (Y - Other.Y)); }
        inline Point operator+(const Point& Other) const { return Point((X + Other.X), (Y + Other.Y)); }
        inline bool operator==(const Point& Other) const { return X == Other.X && Y == Other.Y; }
        
        inline void operator+=(const Point& Other)      { X += Other.X; Y += Other.Y; }
        inline void operator-=(const Point& Other)      { X -= Other.X; Y -= Other.Y; }

        inline bool IsAbove(const Point& P) const       {return Y > P.Y;}
        inline bool IsRightOf(const Point& P) const     {return X > P.X;}
        
        inline static bool Coincident(const Point& P1, const Point& P2)
        {
            return (P1 == P2);
        }

        inline static bool HAligned(const Point& P1, const Point& P2)
        {
            return (P1.Y == P2.Y);
        }

        inline static bool VAligned(const Point& P1, const Point& P2)
        {
            return (P1.X == P2.X);
        }
    };

    struct Rectangle
    {
    protected:
        // Max and Min corners forming this rectangle
        Point MinPoint; // This is always the bottom left corner
        Point MaxPoint; // This is always the top right corner

    public:
        Rectangle()
        : MinPoint(Point())
        , MaxPoint(Point())
        {
        }

        Rectangle(int X1, int Y1, int X2, int Y2);
        Rectangle(const Point& P1, const Point& P2);
        Rectangle(const Rectangle& Other);

        inline int Width() const                { return MaxPoint.X - MinPoint.X; }
        inline int Height() const               { return MaxPoint.Y - MinPoint.Y; }
        inline int Area() const                 { return Width() * Height(); }
        float Diagonal() const;
                                                    
        inline int Bottom() const               { return MinPoint.Y; }
        inline int Top() const                  { return MaxPoint.Y; }
        inline int Left() const                 { return MinPoint.X; }
        inline int Right() const                { return MaxPoint.X; }
                                                  
        inline Point GetTopLeft() const         { return Point(MinPoint.X, MaxPoint.Y); }
        inline Point GetTopRight() const        { return MaxPoint; }
        inline Point GetBottomRight() const     { return Point(MaxPoint.X, MinPoint.Y); }
        inline Point GetBottomLeft() const      { return MinPoint; }
        inline Point GetCenter() const          { return MinPoint + Point(Width()/2, Height()/2); }

        /* Returns true if the formed rectangle is geometrically valid. */
        bool IsValid() const;

        /* Check if the rectangle contains a point */
        bool Contains(const Point& P) const;
        bool Contains(int X, int Y) const;

        /* Test for rectangle overlaps */
        bool Overlaps(const Rectangle& Rect) const;

        /* Clamps a point to restrict it within the rectangle's bound */
        void ClampPoint(Point& P) const;

        /* Rectangle Adjustments */
        void Offset(int Dx, int Dy);
        void SetPosition(const Point& NewPos, EPivot TargetPivot = EPivot::CENTER);
        void SetPosition(int NewX, int NewY, EPivot TargetPivot = EPivot::CENTER);
        void Scale(float scale, EPivot pivot);
        Rectangle GetScaled(float scale, EPivot pivot) const;
    };
}

#endif // !GEOM_2D_H

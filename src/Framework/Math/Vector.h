#pragma once

#ifndef VECTOR_H
#define VECTOR_H

#include "MathUtils.h"

/* 3D and 2D vectors */

/* Should we define Vector2? Should we use Vector3 of 0 Z-Component as a Vector2 instead? Questions that demand answers... */
struct Vector2
{
    float X;
    float Y;

    inline Vector2()
    : X(0.f)
    , Y(0.f)
    {
    }

    inline Vector2(float InX, float InY)
    : X(InX)
    , Y(InY)
    {
    }

    inline Vector2(const Vector2& Other)
    : X(Other.X)
    , Y(Other.Y)
    {
    }

    /* Operator Overloads */
    inline Vector2 operator+(const Vector2& Other) const;
    inline Vector2 operator-(const Vector2& Other) const;
    inline Vector2 operator*(const Vector2& Other) const;
    inline Vector2 operator/(const Vector2& Other) const;

    inline Vector2 operator*(float Scalar) const;
    inline Vector2 operator/(float Scalar) const;

    inline void operator+=(const Vector2& Other);
    inline void operator-=(const Vector2& Other);
    inline void operator*=(const Vector2& Other);
    inline void operator/=(const Vector2& Other);

    /* Equality Operator */
    inline bool operator==(const Vector2& Other) const;

    /* Assignment Operator */
    inline void operator=(const Vector2& Other);

    /* Vector Functions */
    inline static float                 Dot(const Vector2& A, const Vector2& B);
    inline static float                 Distance(const Vector2& A, const Vector2& B);
    inline static float                 AngleBetween(const Vector2& A, const Vector2& B);

    inline float                        Length() const;
    inline float                        LengthSqr() const;

    inline Vector2                      GetNormalized() const;
    inline void                         Normalize();
    inline bool                         ContainsNaN() const;
    inline bool                         IsZero(float Threshold = SMALL_EPSILON) const;

    Vector2                             GetRotated(float degrees);
};

struct Vector3
{
    float X;
    float Y;
    float Z;

    inline Vector3()
    : X(0.f)
    , Y(0.f)
    , Z(0.f)
    {
    }

    inline Vector3(float InX, float InY, float InZ)
    : X(InX)
    , Y(InY)
    , Z(InZ)
    {
    }

    inline Vector3(const Vector3& Other)
    : X(Other.X)
    , Y(Other.Y)
    , Z(Other.Z)
    {
    }

    inline Vector3(const Vector2& V2, float InZ = 0.f)
    : X(V2.X)
    , Y(V2.Y)
    , Z(InZ)
    {
    }

    /* Common Static Members */
    //static Vector3 ZeroVector;
    //static Vector3 UpVector;
    //static Vector3 ForwardVector;
    //static Vector3 RightVector;

    /* Operator Overloads */
    inline Vector3 operator+(const Vector3& Other) const;
    inline Vector3 operator-(const Vector3& Other) const;
    inline Vector3 operator*(const Vector3& Other) const;
    inline Vector3 operator/(const Vector3& Other) const;

    inline Vector3 operator*(float Scalar) const;
    inline Vector3 operator/(float Scalar) const;

    inline void operator+=(const Vector3& Other);
    inline void operator-=(const Vector3& Other);
    inline void operator*=(const Vector3& Other);
    inline void operator/=(const Vector3& Other);

    /* Equality Operator */
    inline bool operator==(const Vector3& Other) const;
    
    /* Assignment Operator */
    inline void operator=(const Vector3& Other);

    /* Vector Functions */
    inline static float                 Dot(const Vector3& A, const Vector3& B);
    inline static Vector3               Cross(const Vector3& A, const Vector3& B);
    inline static float                 Distance(const Vector3& A, const Vector3& B);
    inline static float                 Distance2D(const Vector3& A, const Vector3& B);
    inline static float                 AngleBetween(const Vector3& A, const Vector3& B);
    inline static float                 AngleBetween2D(const Vector3& A, const Vector3& B); // This function seems redundant, just pass vectors with 0 Z component to AngleBetween

    inline float                        Length() const;
    inline float                        Length2D() const;
    inline float                        LengthSqr() const;
    inline float                        CosineAngle2D(Vector3 B) const;

    inline Vector3                      GetNormalized() const;
    inline void                         Normalize();
    inline bool                         ContainsNaN() const;
    inline bool                         IsZero(float Threshold = SMALL_EPSILON) const;

    Vector3                             GetRotated(const Vector3& Axis, float degrees);
};

/*============================================================*
                            Vector2
*============================================================*/
inline Vector2 Vector2::operator+(const Vector2& Other) const
{
    return (Vector2(X + Other.X, Y + Other.Y));
}

inline Vector2 Vector2::operator-(const Vector2& Other) const
{
    return (Vector2(X - Other.X, Y - Other.Y));
}

inline Vector2 Vector2::operator*(const Vector2& Other) const
{
    return (Vector2(X * Other.X, Y * Other.Y));
}

inline Vector2 Vector2::operator/(const Vector2& Other) const
{
    if(Other.ContainsNaN())
    {
        return Vector2();
    }

    return (Vector2(X / Other.X, Y / Other.Y));
}

inline Vector2 Vector2::operator*(float Scalar) const
{
    return (Vector2(X * Scalar, Y * Scalar));
}

inline Vector2 Vector2::operator/(float Scalar) const
{
    return (Vector2(X / Scalar, Y / Scalar));
}


inline void Vector2::operator+=(const Vector2& Other)
{
    X += Other.X;
    Y += Other.Y;
}

inline void Vector2::operator-=(const Vector2& Other)
{
    X -= Other.X;
    Y -= Other.Y;
}

inline void Vector2::operator*=(const Vector2& Other)
{
    X *= Other.X;
    Y *= Other.Y;
}

inline void Vector2::operator/=(const Vector2& Other)
{
    X /= Other.X;
    Y /= Other.Y;
}

inline bool Vector2::operator==(const Vector2& Other) const
{
    return (X == Other.X
        && Y == Other.Y);
}
inline void Vector2::operator=(const Vector2& Other)
{
    X = Other.X;
    Y = Other.Y;
}

inline float Vector2::Dot(const Vector2& A, const Vector2& B)
{
    return (A.X * B.X + A.Y + B.Y);
}

inline float Vector2::Distance(const Vector2& A, const Vector2& B)
{
    return (B-A).Length();
}

inline float Vector2::AngleBetween(const Vector2& A, const Vector2& B)
{
    const float dot = Vector2::Dot(A, B);

    return Math::ACosFast(dot / (A.Length() * B.Length()));
}

inline float Vector2::Length() const
{
    return Math::Sqrt(X*X + Y*Y);
}

inline float Vector2::LengthSqr() const
{
    return (X*X + Y*Y);
}

inline Vector2 Vector2::GetNormalized() const
{
    if(IsZero())
    {
        return Vector2();
    }

    const float scale = Math::InvSqrt(X*X + Y*Y);

    return (Vector2(X, Y) * scale);
}

inline void Vector2::Normalize()
{
    if(IsZero())
    {
        X = 0;
        Y = 0;
    }

    *this * Math::InvSqrt(X*X + Y*Y);
}

inline bool Vector2::ContainsNaN() const
{
    return Math::IsNaN(X) || Math::IsNaN(Y);
}

inline bool Vector2::IsZero(float Threshold) const
{
    return Length() < Threshold;
}

inline Vector2 Vector2::GetRotated(float degrees)
{
    return Vector2( X*Math::Cosd(degrees) - Y*Math::Sind(degrees),
                    X*Math::Sin(degrees) + Y*Math::Cosd(degrees));
}

/*============================================================*
                            Vector3
 *============================================================*/
inline Vector3 Vector3::operator+(const Vector3& Other) const
{
    return (Vector3(X + Other.X, Y + Other.Y, Z + Other.Z));
}

inline Vector3 Vector3::operator-(const Vector3& Other) const
{
    return (Vector3(X - Other.X, Y - Other.Y, Z - Other.Z));
}

inline Vector3 Vector3::operator*(const Vector3& Other) const
{
    return (Vector3(X * Other.X, Y * Other.Y, Z * Other.Z));
}

inline Vector3 Vector3::operator/(const Vector3& Other) const
{
    if(Other.ContainsNaN())
    {
        return Vector3();
    }

    return (Vector3(X / Other.X, Y / Other.Y, Z / Other.Z));
}

inline Vector3 Vector3::operator*(float Scalar) const
{
    return (Vector3(X * Scalar, Y * Scalar, Z * Scalar));
}

inline Vector3 Vector3::operator/(float Scalar) const
{
    return (Vector3(X / Scalar, Y / Scalar, Z / Scalar));
}


inline void Vector3::operator+=(const Vector3& Other)
{
    X += Other.X;
    Y += Other.Y;
    Z += Other.Z;
}

inline void Vector3::operator-=(const Vector3& Other)
{
    X -= Other.X;
    Y -= Other.Y;
    Z -= Other.Z;
}

inline void Vector3::operator*=(const Vector3& Other)
{
    X *= Other.X;
    Y *= Other.Y;
    Z *= Other.Z;
}

inline void Vector3::operator/=(const Vector3& Other)
{
    X /= Other.X;
    Y /= Other.Y;
    Z /= Other.Z;
}

inline bool Vector3::operator==(const Vector3& Other) const
{
    return (X == Other.X
         && Y == Other.Y
         && Z == Other.Z);
}
inline void Vector3::operator=(const Vector3& Other)
{
    X = Other.X;
    Y = Other.Y;
    Z = Other.Z;
}

/* Vector Functions */
inline float Vector3::Dot(const Vector3& A, const Vector3& B)
{
    return (A.X * B.X
          + A.Y * B.Y
          + A.Z * B.Z);
}

inline Vector3 Vector3::Cross(const Vector3& A, const Vector3& B)
{
    return Vector3( A.Y * B.Z - A.Z * B.Y,		// X-Component
                    A.Z * B.X - A.X * B.Z,		// Y-Component
                    A.X * B.Y - A.Y * B.X);		// Z-Component
}

inline float Vector3::Distance(const Vector3& A, const Vector3& B)
{
    return (B - A).Length();
}

inline float Vector3::Distance2D(const Vector3& A, const Vector3& B)
{
    return (B - A).Length2D();
}

inline float Vector3::AngleBetween(const Vector3& A, const Vector3& B)
{
    const float dot = Vector3::Dot(A, B);

    return Math::ACosFast( dot/ (A.Length() * B.Length()) );
}

inline float Vector3::AngleBetween2D(const Vector3& A, const Vector3& B)
{
    const Vector3 A2 = Vector3(A.X, A.Y, 0.f);
    const Vector3 B2 = Vector3(B.X, B.Y, 0.f);

    const float dot = Vector3::Dot(A,B);

    return Math::ACosFast( dot/(A.Length2D() * B.Length2D()) );
}


inline float Vector3::Length() const
{
    return Math::Sqrt(X*X + Y*Y + Z*Z);
}

inline float Vector3::Length2D() const
{
    return Math::Sqrt(X*X + Y*Y);
}

inline float Vector3::LengthSqr() const
{
    return (X*X + Y*Y + Z*Z);
}

inline float Vector3::CosineAngle2D(Vector3 B) const
{
    Vector3 A(*this);
    A.Z = 0.0f;
    B.Z = 0.0f;
    A.Normalize();
    B.Normalize();
    return Vector3::Dot(A , B);
}

// This operation is unsafe
inline Vector3 Vector3::GetNormalized() const
{
    if(IsZero())
    {
        return Vector3();
    }

    const float scale = Math::InvSqrt(X*X + Y*Y + Z*Z);

    return (Vector3(X,Y,Z) * scale);
}

// This operation is unsafe
inline void Vector3::Normalize()
{
    if(IsZero())
    {
        X = 0;
        Y = 0;
        Z = 0;
    }

    *this * Math::InvSqrt(X*X + Y*Y + Z*Z);
}

inline bool Vector3::ContainsNaN() const
{
    return (   Math::IsNaN(X)
            || Math::IsNaN(Y)
            || Math::IsNaN(Z));
}

inline bool Vector3::IsZero(float Threshold) const
{
    const float length = Length();
    return length < Threshold;
}
inline Vector3 Vector3::GetRotated(const Vector3& Axis, float degrees)
{
    const Vector3 axis = Axis.GetNormalized();
    const float ux = axis.X;
    const float uy = axis.Y;
    const float uz = axis.Z;

    const float cos = Math::Cosd(degrees);
    const float sin = Math::Sind(degrees);

    const float x = (cos + (ux*ux)*(1-cos)) + (ux*uy*(1-cos) - uz*sin) + (ux*uz*(1-cos) + uy*sin);
    const float y = (uy*ux*(1-cos) + uz*sin) + (cos + uy*uy*(1-cos)) + (uy*uz*(1-cos) - ux*sin);
    const float z = (uz*uz*(1-cos) - uy*sin) + (uz*uy*(1-cos) + ux*sin) + (cos + uz*uz*(1-cos));

    return Vector3(x,y,z);
}
#endif // VECTOR_H
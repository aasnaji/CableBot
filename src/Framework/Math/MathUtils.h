#pragma once

#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#ifndef  M_PI
    #define M_PI            (3.1415926535897f)
#endif

#define M_HALF_PI       (1.57079632679f)
#define M_QUARTER_PI    (0.785398163397f)
#define M_INV_PI	    (0.31830988618f)

#define EPSILON           (0.00001f)
#define SMALL_EPSILON     (0.00000001f)
#define TINY_EPSILON      (0.00000000001f)
#define LARGE_EPSILON     (0.001f)
#define HUGE_EPSILON      (0.1f)

/*  NOTE:
*   We could choose to template most of these functions - generally, that would be
*   desirable. But when working with constrained hardware requirements, it is best
*   we stay disciplined and assume all calculations are on floats. Otherwise, program
*   size will be bloated to oblivion.
*/

namespace Math
{
    inline static float Abs(float Num)
    {
        return (Num < 0) ? -Num : Num;
    }

    /* Linear Interpolation between two numeric values. Note this function is worth templating because it applies to non-POD types*/
    template<typename T, typename U>
    inline static T Lerp(const T& A, const T& B, const U& Alpha)
    {
        return (T)(A + Alpha * (B - A));
    }

    inline static float Clamp(float Min, float Max, float Num)
    {
        return Num < Min ? Min : Num > Max ? Max : Num;
    }

    inline static float ToRadians(float Degrees)
    {
        return Degrees * M_PI / 180;
    }

    inline static float ToDegrees(float Rads)
    {
        return Rads * 180 / M_PI;
    }

    inline static float Max(float A, float B)
    {
        return (A >= B ? A : B);
    }

    inline static int Max(int A, int B)
    {
        return (A >= B ? A : B);
    }
    
    inline static float Max3(float A, float B, float C)
    {
        return Max(Max(A,B), C);
    }

    inline static int Max3(int A, int B, int C)
    {
        return Max(Max(A, B), C);
    }

    inline static float Min(float A, float B)
    {
        return (A <= B ? A : B);
    }

    inline static int Min(int A, int B)
    {
        return (A <= B ? A : B);
    }

    inline static float Min3(float A, float B, float C)
    {
        return Min(Min(A,B), C);
    }

    inline static int Min3(int A, int B, int C)
    {
        return Min(Min(A, B), C);
    }

    inline static bool IsNearlyEqual(float A, float B, float Threshold = SMALL_EPSILON)
    {
        return (A == B) || (Abs(A-B) < Threshold);
    }

    inline static bool IsFinite(float Num)
    {
        // Override this later
        return true;
    }

    inline static bool IsNaN(float Num)
    {
        // Override this later
        return false;
    }
    /*	This code is a direct copy of the implementation of fast inverse square root
    *	from the developers of Quake Arena. I take no credits for it.
    *
    *	The algorithm uses a hack followed by a 1-step Newtwon approximation for improved
    *	results. More can be found here: https://www.geeksforgeeks.org/fast-inverse-square-root/
    */
    inline static float InvSqrt(float Num)
    {
        const float threehalfs = 1.5F;

        float x2 = Num * 0.5F;
        float y = Num;

        // evil floating point bit level hacking 
        long i = *(long *)&y;

        // value is pre-assumed 
        i = 0x5f3759df - (i >> 1);
        y = *(float *)&i;

        // 1st iteration 
        y = y * (threehalfs - (x2 * y * y));

        // 2nd iteration, this can be removed 
        // y = y * ( threehalfs - ( x2 * y * y ) ); 

        return y;
    }

    inline static float Sqrt(float Num)
    {
        if(Num == 0)
        {
            return 0;
        }

        return (1/InvSqrt(Num));
    }

    /* Returns the inverse cosine in units radians */
    inline float ACosFast(float num)
    {
        float negate = float(num < 0.f);
        num = Abs(num);
        float ret = -0.0187293f;
        ret = ret * num;
        ret = ret + 0.0742610f;
        ret = ret * num;
        ret = ret - 0.2121144f;
        ret = ret * num;
        ret = ret + 1.5707288f;
        ret = ret * Sqrt(1.0f - num);
        ret = ret - 2.f * negate * ret;
        return negate * 3.14159265358979f + ret;
    }

    inline float Cos(float rad)
    {
        // @TODO: Use math library default
        return 0;
    }

    inline float Cosd(float deg)
    {
        // @TODO: Use math library default
        return Cos(ToRadians(deg));
    }

    inline float Sin(float rad)
    {
        // @TODO: Use math library default
        return 0;
    }

    inline float Sind(float deg)
    {
        // @TODO: Use math library default
        return Sin(ToRadians(deg));
    }
}

#endif // !MATH_UTILS_H
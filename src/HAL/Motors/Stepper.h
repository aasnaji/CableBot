#pragma once
#ifndef STEPPER_H
#define STEPPER_H

#include "../Core.h"
#include "../IMotor.h"

#include <stdint.h>

namespace Motor
{
    enum class EMicroStep
    {
        MS_1    = (1 << 0),
        MS_2    = (1 << 1),
        MS_4    = (1 << 2),
        MS_8    = (1 << 3),
        MS_16   = (1 << 4),
        MS_32   = (1 << 5),
        MS_64   = (1 << 6),
        MS_128  = (1 << 7)
    };

    class Stepper : public IMotor
    {
        MAKE_EVENT(MotorHaltCallback, Stepper*);
        MAKE_EVENT(MotorCurrentCallback, Stepper*, float);
        MAKE_EVENT(MotorStepReachedCallback, Stepper*, long);

    public:
        virtual ~Stepper() {}

        static Stepper* Create(short steps, short dirPin, short stepPin, size_t auxPinCount = 0 , const short* auxillaryPins = nullptr);

        /* Movement functions for steppers using step counts or unit distances */
        virtual void MoveSteps(short step, bool bBlocking) = 0;
        virtual void MoveDist(float distance, bool bBlocking) = 0;

        /* Returns the steps per revolution of the motor. This is based on the motor's specs */
        virtual short GetStepsPerRevolution() const = 0;

        /* Returns the steps per unit of distance. This is helpful for controlling the stepper in distance based movements */
        virtual short GetStepsPerDistance() const = 0;
        virtual void  SetStepsPerDistance(short spd) = 0;

        /* Sets the microstepping mode for the stepper if allowed. 
        *  The actual result will be clamped if the setting exceeds the motor's capability */
        virtual void SetMicroStep(EMicroStep ms) = 0;

        /* Returns the current step count - this is indicative of "position". */
        virtual long GetAccumulatedStep() const = 0;
    protected:
    private:
    };
#endif // !STEPPER_H


}
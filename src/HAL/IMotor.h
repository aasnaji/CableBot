#pragma once
#ifndef IMOTOR_H
#define IMOTOR_H

namespace Motor
{
    static const short PIN_UNUSED = -1;

    class IMotor
    {
    public:
        virtual ~IMotor() {}
    
        virtual void Update(float deltaTime) {}

        // In the controller, we would want a positional version as such. We restrict to rotational for now because it is common to all motors
        // virtual void     Move(float delta, float time = 0, bool bBlocking = false);

        virtual void        Rotate(float deg, bool bBlocking = false) = 0;
        virtual void        BrakeStop() = 0;
        virtual void        ImmediateStop() = 0;

        virtual void        SetRPM(float rpm) = 0;
        virtual float       GetRPM() const = 0;

        virtual bool        IsValid() const = 0;
        virtual void        SetEnabled(bool bEnabled) = 0;

        virtual void        OnAttach() {}
        virtual void        OnDetach() {}
    protected:
    private:
    };
}
#endif // !IMOTOR_H
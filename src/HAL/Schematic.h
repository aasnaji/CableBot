#pragma once
#ifndef SCHEMATIC_H
#define SCHEMATIC_H

/*  Abstraction that represent the hardware schematic and mapping of 
*   the robot core. The schematic is "loaded" by the robot core to
*   automatic initialization of a robotic setup without the use of client code.
*/
struct FSchematic
{
private:
    unsigned char* binary;
};
#endif // !SCHEMATIC_H

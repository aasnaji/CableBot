#include "Core.h"

#include "Arduino.h"
#include "Print.h"

#include "RobotInstance.h"

void LogAndAbort(const char* msg, const char* inFile, int inLine)
{
    Serial.print("Assertion in file '"); Serial.print(inFile); Serial.print("' @ Line "); Serial.println(inLine);
    if(msg)
        Serial.println(msg);

    Serial.flush();

    delay(10);

    if(RobotInstance* robot = RobotInstance::GetInstance())
    {
        //robot->Shutdown();
    }

    abort();
}

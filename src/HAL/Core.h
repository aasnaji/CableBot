#pragma once
#ifndef CORE_H
#define CORE_H

#include <stdint.h>
#include <stddef.h>

/******************************************************************************************/
/*                                 PLATFORM DEFINITIONS                                   */
/******************************************************************************************/

// Temporarily enforced for debugging, otherwise we won't compile (which is pretty cool)
//#define SS_PLATFORM_ARDUINO
//#define UBRRH  // This force enables Serial

#define SS_DEBUG

#if defined (AVR) || defined (__AVR)
    #define SS_PLATFORM_ARDUINO
#endif // (AVR || __AVR)


#ifdef SS_PLATFORM_ARDUINO
    void LogAndAbort(const char* msg, const char* inFile, int inLine);

    #ifdef SS_DEBUG
        #define check(expr)		        if(!(expr)) LogAndAbort( nullptr ,(__FILE__), (__LINE__) );
        #define checkf(expr, msg)       if(!(expr)) LogAndAbort( msg ,(__FILE__), (__LINE__) );
        #define verify(expr)            if(!(expr)) LogAndAbort( nullptr, (__FILE__), (__LINE__) );
        #define verifyf(expr, msg)      if(!(expr)) LogAndAbort( msg, (__FILE__), (__LINE__) );
    #else
        #define check(expr)
        #define checkf(expr, msg)
        #define verify(expr)            if(!(expr)) LogAndAbort( nullptr, (__FILE__), (__LINE__) );
        #define verifyf(expr, msg)      if(!(expr)) LogAndAbort( msg, (__FILE__), (__LINE__) );
    #endif // SS_DEBUG
#elif SS_PLATFORM_RPI

#endif // SS_PLATFORM_ARDUINO


#if !defined (SS_PLATFORM_ARDUINO) && !defined (SS_PLATFORM_RPI)
    static_assert(false, "No Valid Target Platform is being compiled");
#endif

/******************************************************************************************/
/*                                      CORE MACROS                                       */
/******************************************************************************************/
#define FWD_DECLARE_ENUM(EnumType)    enum EnumType : unsigned char;
#define DECL_ENUM(EnumType, ...)      namespace EnumType { enum Type : unsigned char {__VA_ARGS__, MAX_VAL}; }

/* Generate callback mechanism declarations to perform event handling 
*  For expected behavior, please try to and make sure this is the first set of declarations in your class.
*/
#define MAKE_EVENT(Name, ...)                   \
public: typedef void(*Name)(__VA_ARGS__);       \
void Set##Name(Name cb) {m_##Name = cb;}        \
private: Name m_##Name = nullptr;                       

#endif // !CORE_H

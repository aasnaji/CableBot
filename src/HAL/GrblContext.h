#pragma once
#ifndef GRLB_CONTEXT_H
#define GRLB_CONTEXT_H

#include "Core.h"

#define COORD_STR(coord)        (coord == Modals::ECoordinate::Local ? "G52" : coord == Modals::ECoordinate::Machine ? "G53" : "G16")
#define UNITS_STR(units)        (units == Modals::EUnits::Metric ? "G21" : "G20")
#define DIST_STR(dist)          (dist == Modals::EDistancing::Absolute ? "G90" : "G91")
#define MOTION_STR(motion)      (motion == Modals::EMotion::Rapid ? "G00" : "G01")

namespace Modals
{
    enum class ECoordinate : short
    {
        Local = 0x0,
        Machine = 0x1,
        Polar = 0x2
    };

    enum class EUnits : short
    {
        Metric = 0x0,
        Imperial = 0x1
    };

    enum class EDistancing : short
    {
        Absolute = 0x0,
        Incremental = 0x1
    };

    enum class EMotion : short
    {
        Rapid = 0x0,
        Lerp = 0x1
        // Circular, etc..
    };
}


class GRBLContext
{
public:
    enum class EAxis
    {
        X,Y,Z
    };

    struct GRBLInitInfo
    {
        // Steps Per Distance (steps/mm)
        float UnitStepX;
        float UnitStepY;
        float UnitStepZ;

        // Accel (mm/sec^2)
        float AccelX;
        float AccelY;
        float AccelZ;

        // Max Speed (mm/min)
        float MaxRateX;
        float MaxRateY;
        float MaxRateZ;

        // Max travel distance (mm)
        float MaxRangeX;
        float MaxRangeY;
        float MaxRangeZ;
    };

    static GRBLContext* Create(const GRBLInitInfo& initInfo);

    /* Movement commands for motors. I set this to pure virtual because we may want to exploit
    *  hardware specific serialization of data. Example: Instead of converting a float to a string,
    *  simply pass it to Arduino's serial directly without conversions.
    */
    virtual void MoveAxis(EAxis axis, float val) = 0;
    virtual void MoveAxes(EAxis axis1, EAxis axis2, float val1, float val2) = 0;
    virtual void MoveAxes(float valX, float valY, float valZ) = 0;

    inline void                     SetCoordinates(Modals::ECoordinate coordinate)         { m_Coordinate = static_cast<unsigned short>(coordinate); }
    inline void                     SetUnits(Modals::EUnits units)                         { m_Units = static_cast<unsigned short>(units); }
    inline void                     SetDistancing(Modals::EDistancing distancing)          { m_Distancing = static_cast<unsigned short>(distancing); }
    inline void                     SetMotion(Modals::EMotion motion)                      { m_Motion = static_cast<unsigned short>(motion); }

    // Query methods for G-Code modal status
    inline Modals::ECoordinate      GetCoordinates() const          { return static_cast<Modals::ECoordinate>(m_Coordinate); }
    inline Modals::EUnits           GetUnits() const                { return static_cast<Modals::EUnits>(m_Units); }
    inline Modals::EDistancing      GetDistancing() const           { return static_cast<Modals::EDistancing>(m_Distancing); }
    inline Modals::EMotion          GetMotion() const               { return static_cast<Modals::EMotion>(m_Motion); }
protected:
    inline char GetAxisChar(EAxis axis) const
    {
        return axis == EAxis::X ? 'X' : axis == EAxis::Y ? 'Y' : 'Z';
    }

    // DEPRECATED
    virtual void PushCommand(const char* cmd) const {}

private:
    // For 4 modals, simply need 1 byte.
    unsigned short m_Coordinate : 2;
    unsigned short m_Units : 1;
    unsigned short m_Distancing : 1;
    unsigned short m_Motion : 4; // 4 to fit padding

};
#endif // !GRLB_CONTEXT_H

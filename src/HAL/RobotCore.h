#pragma once
#ifndef ROBOT_CORE_H
#define ROBOT_CORE_H

#include "Core.h"

#include "IMotor.h"
#include "Schematic.h"

#include "../Framework/Math/Vector.h"

namespace Motor
{
    class IMotor;
}

namespace Sensor
{
    class ISensor;
}

/*  A representation of a platform independent core for a robot device.
*   Acts as a unified construct to program robotic applications using
*   motors and sensors regardless of hardware interfacing. Since programs
*   are loaded on a per hardware manner, there can technically only be one
*   instance of a RobotCore throughout an application's lifetime; hence the
*   enforced singleton instancing. Attempting to create multiple robot cores
*   will throw an assertion at runtime.
*/
class RobotCore
{
public:
    static RobotCore* Create();

    virtual void Init() = 0;
    virtual void Shutdown() = 0;
    virtual void Update(float deltaTime) = 0;

    virtual float GetTime() const = 0;
    virtual unsigned long GetTimeMilli() const = 0;
    virtual unsigned long GetTimeMicro() const = 0;

    virtual void Log(const char* msg) = 0;

    virtual Motor::IMotor* GetMotor(unsigned char id) const = 0;
    virtual Sensor::ISensor* GetSensor(unsigned char id) const = 0;

    /* These are typically used to add auxillary hardware on top of the defaults. An assertion is thrown if no hardware can be further fitted/added
    *  The hardware component is returned as a reference for client side post initialization.
    */
    template<typename T, typename...ConstructArgs>
    T&  AttachMotor(ConstructArgs...args)
    {
        T* motor = T::Create(args...);
        verify(AttachMotorInternal(motor));

        return *motor;
    }

    /* Variant to attach already instantiated motor. This implies ownership transfer to the RobotCore! */
    template<typename T>
    T& AttachMotor(T* motor)
    {
        verify(AttachMotorInternal(motor));

        return *motor;
    }

    template<typename T, typename...ConstructArgs>
    T& AttachSensor(ConstructArgs...args)
    {
        T* sensor = new T(args...);
        verify(AttachSensorInternal(sensor));

        return *sensor;
    }

    template<typename T>
    T& AttachSensor(T* sensor)
    {
        verify(AttachSensorInternal(sensor));

        return *sensor;
    }

protected:
    // Return false if the attachment fails. This must be asserted on, we cannot have cases where hardware is arbitrarily aborted on
    // These functions internally add the hardware components for further management
    virtual bool AttachMotorInternal(Motor::IMotor* motor) = 0;
    virtual bool AttachSensorInternal(Sensor::ISensor* sensor) = 0;

    virtual void LoadSchematic(const FSchematic& schematic) {}

    // Cannot be constructed - Use Factory method
    RobotCore(){}
protected:
private:
};
#endif // !ROBOT_CORE_H

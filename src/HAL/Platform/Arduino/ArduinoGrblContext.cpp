#include "ArduinoGrblContext.h"
#include "HardwareSerial.h"

/*  These are here for your reference. We do not want to use them because otherwise 
*   we will have to format them into strings - hard to read, and worse performance.*/

#define STEPS_PER_MILLI_X       "$100"
#define STEPS_PER_MILLI_Y       "$101"
#define STEPS_PER_MILLI_Z       "$102"
#define MAX_RATE_X              "$110"
#define MAX_RATE_Y              "$111"
#define MAX_RATE_Z              "$112"
#define ACCEL_X                 "$120"
#define ACCEL_Y                 "$121"
#define ACCEL_Z                 "$122"
#define MAX_TRAVEL_X            "$130"
#define MAX_TRAVEL_Y            "$131"
#define MAX_TRAVEL_Z            "$132"



#ifdef SS_PLATFORM_ARDUINO
GRBLContext* GRBLContext::Create(const GRBLInitInfo& initInfo)
{
    return new ArduinoGRBLContext(initInfo);
}
#endif // SS_PLATFORM_ARDUINO

ArduinoGRBLContext::ArduinoGRBLContext(const GRBLInitInfo& initInfo)
{
    Serial.flush();

    char settingsCmd[100];

    int success = sprintf(settingsCmd, R"($100=%f
                                        $101=%f
                                        $102=%f
                                        $110=%f
                                        $111=%f
                                        $112=%f
                                        $120=%f
                                        $121=%f
                                        $122=%f
                                        $130=%f
                                        $131=%f
                                        $132=%f)"
                                        , initInfo.UnitStepX, initInfo.UnitStepY, initInfo.UnitStepZ
                                        , initInfo.MaxRateX, initInfo.MaxRateY, initInfo.MaxRateZ
                                        , initInfo.AccelX, initInfo.AccelY, initInfo.AccelZ
                                        , initInfo.MaxRangeX, initInfo.MaxRangeY, initInfo.MaxRangeZ);
    verify(success);

    // Execute that bitch!
    Serial.print(settingsCmd);
    Serial.flush();
}

void ArduinoGRBLContext::MoveAxis(EAxis axis, float val)
{
    const char _axis = GetAxisChar(axis);

    // Arbitrary sizing to support float decimal points
    char cmd[30];
    int success = sprintf(cmd, "%s%s%s%c%f.", DIST_STR(GetDistancing()), COORD_STR(GetCoordinates()), MOTION_STR(GetMotion()), _axis, val);
    verify(success);

    Serial.print(cmd);
    Serial.flush();
}

void ArduinoGRBLContext::MoveAxes(EAxis axis1, EAxis axis2, float val1, float val2)
{
    const char _axis1 = GetAxisChar(axis1);
    const char _axis2 = GetAxisChar(axis2);

    // Arbitrary sizing to support float decimal points
    char cmd[30];
    int success = sprintf(cmd, "%s%s%s%c%f.%c%f.", DIST_STR(GetDistancing()), COORD_STR(GetCoordinates()), MOTION_STR(GetMotion()), _axis1, val1, _axis2, val2);
    verify(success);

    Serial.print(cmd);
    Serial.flush();
}

void ArduinoGRBLContext::MoveAxes(float valX, float valY, float valZ)
{
    // Arbitrary sizing to support float decimal points
    char cmd[30];
    int success = sprintf(cmd, "%s%s%sX%f.Y%f.Z%f", DIST_STR(GetDistancing()), COORD_STR(GetCoordinates()), MOTION_STR(GetMotion()), valX, valY, valZ);
    verify(success);

    Serial.print(cmd);
    Serial.flush();
}

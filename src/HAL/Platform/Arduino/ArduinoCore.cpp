#include "ArduinoCore.h"

#ifdef SS_PLATFORM_ARDUINO
RobotCore* RobotCore::Create()
{
    return new ArduinoCore();
}
#endif // SS_PLATFORM_ARDUINO

ArduinoCore::ArduinoCore()
{
    for(int i = EMotorSlots::SLOT1; i != EMotorSlots::MAX_VAL; i++)
    {
        m_Motors[i] = nullptr;
    }
}

void ArduinoCore::Init()
{
    // Hardware Init timing window
    delay(10);

    // Serial setup
    Serial.begin(9600);
    Serial.println("Hello from Arduino Core!");
}

void ArduinoCore::Shutdown()
{
    for(int i = EMotorSlots::SLOT1; i != EMotorSlots::MAX_VAL; i++)
    {
        DetachMotor(static_cast<EMotorSlots::Type>(i));
    }

    for(int i = ESensorSlots::SLOT1; i != ESensorSlots::MAX_VAL; i++)
    {
        // Detach sensor...
    }
}

void ArduinoCore::Update(float deltaTime)
{
    for(Motor::IMotor* motor : m_Motors)
    {
        motor->Update(deltaTime);
    }

    for(Sensor::ISensor* sensor : m_Sensors)
    {
        // Update
    }
}

void ArduinoCore::Log(const char* msg)
{
#ifdef SS_DEBUG
    Serial.println(msg);
#endif // SS_DEBUG    
}

float ArduinoCore::GetTime() const
{
    return (millis() / 1000.f);
}

unsigned long ArduinoCore::GetTimeMilli() const
{
    return millis();
}

unsigned long ArduinoCore::GetTimeMicro() const
{
    return micros();
}

Motor::IMotor* ArduinoCore::GetMotor(unsigned char id) const
{
    if(id >= EMotorSlots::MAX_VAL)
        return nullptr;

    return m_Motors[id];
}

Sensor::ISensor* ArduinoCore::GetSensor(unsigned char id) const
{
    if(id >= ESensorSlots::MAX_VAL)
        return nullptr;

    return m_Sensors[id];
}

void ArduinoCore::DetachMotor(EMotorSlots::Type slot)
{
    Motor::IMotor* motor = m_Motors[slot];

    // Do detachment code... 

    // Call motor handler
    if(motor)
    {
        motor->OnDetach();
    }

    delete motor;
    motor = nullptr;
    m_Motors[slot] = nullptr;

    // Destroy the motor
}

void ArduinoCore::DetachSensor(ESensorSlots::Type slot)
{
}

// Gotta skimp on program space yo
static const char* attachFailMsg = "Hardware attachment failed: No slot available";

bool ArduinoCore::AttachMotorInternal(Motor::IMotor* motor)
{
    if(!motor)
    {
        checkf(false, attachFailMsg);
        return false;
    }

    for(int i = EMotorSlots::SLOT1; i != EMotorSlots::MAX_VAL; i++)
    {
        if(!m_Motors[i])
        {
            m_Motors[i] = motor;
            return true;
        }
    }
    
    checkf(false, attachFailMsg);
    return false;
}

bool ArduinoCore::AttachSensorInternal(Sensor::ISensor* sensor)
{
    if(!sensor)
    {
        checkf(false, attachFailMsg);
        return false;
    }

    for(int i = ESensorSlots::SLOT1; i != EMotorSlots::MAX_VAL; i++)
    {
        if(!m_Sensors[i])
        {
            m_Sensors[i] = sensor;
            return true;
        }
    }
    
    checkf(false, attachFailMsg);
    return false;
}

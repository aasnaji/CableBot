#pragma once
#ifndef ARDUINO_GRBL_CONTEXT_H
#define ARDUINO_GRBL_CONTEXT_H

#include "../../GrblContext.h"
#include "../../../../vendor/grbl/grbl/grbl.h"

class ArduinoGRBLContext : public GRBLContext
{
public:
    ArduinoGRBLContext(const GRBLInitInfo& initInfo);
    
    virtual void MoveAxis(EAxis axis, float val) override;
    virtual void MoveAxes(EAxis axis1, EAxis axis2, float val1, float val2) override;
    virtual void MoveAxes(float valX, float valY, float valZ) override;
};
#endif // !ARDUINO_GRBL_CONTEXT_H
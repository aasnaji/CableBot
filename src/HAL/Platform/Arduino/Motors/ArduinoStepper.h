#pragma once
#ifndef ARDUINO_STEPPER_H
#define ARDUINO_STEPPER_H

#include "../../../Motors/Stepper.h"

// I am using this kind of gross include because there is no guarantee an Arduino project can make use of things such as include directories
// This may change if proven otherwise
#include "../../../../../vendor/StepperDriver/src/DRV8825.h"

namespace Motor
{
    class ArduinoStepper : public Stepper
    {
    public:
        ArduinoStepper(short steps, short dirPin, short stepPin, short auxillaryPinCount, const short* auxillaryPins);
        ~ArduinoStepper(){}

        virtual void Update(float deltaTime) override;

        /* Stepper Interface */
        virtual void MoveSteps(short step, bool bBlocking = false) override;
        virtual void MoveDist(float distance, bool bBlocking) override;

        virtual short GetStepsPerRevolution() const override;
        virtual short GetStepsPerDistance() const override;
        virtual void  SetStepsPerDistance(short spd) override;
        virtual long GetAccumulatedStep() const override;
        virtual void SetMicroStep(EMicroStep ms) override;

        /* Motor Interface */
        virtual void        Rotate(float deg, bool bBlocking = false) override;
        virtual void        BrakeStop() override;
        virtual void        ImmediateStop() override;

        virtual void        SetRPM(float rpm) override;
        virtual float       GetRPM() const override;

        virtual bool        IsValid() const override;
        virtual void        SetEnabled(bool bEnabled) override;
    protected:

    private:
        DRV8825 m_Stepper;

        short m_StepsPerDistance;
        bool m_NeedsUpdate;
    };
}
#endif // !ARDUINO_STEPPER_H

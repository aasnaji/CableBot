#include "ArduinoStepper.h"

using namespace Motor;

#define INIT_IF_EXIST(pinIdx) (auxPinCount-1 >= pinIdx ? auxillaryPins[pinIdx] : -1 )

#ifdef SS_PLATFORM_ARDUINO
    Stepper* Stepper::Create(short steps, short dirPin, short stepPin, size_t auxPinCount, const short* auxillaryPins)
    {
        return new ArduinoStepper(steps, stepPin, dirPin, auxPinCount, auxillaryPins);
    }
#endif // SS_PLATFORM_ARDUINO

ArduinoStepper::ArduinoStepper(short steps, short dirPin, short stepPin, short auxPinCount, const short* auxillaryPins)
    : m_Stepper(DRV8825(steps, dirPin, stepPin, INIT_IF_EXIST(0)))
    , m_StepsPerDistance(0)
    , m_NeedsUpdate(false)
{
    m_Stepper.begin();
}

void ArduinoStepper::Update(float deltaTime)
{
    Stepper::Update(deltaTime);

    if(m_NeedsUpdate)
    {
        if(m_Stepper.nextAction() <= 0)
            m_NeedsUpdate = false;
    }
}

void ArduinoStepper::MoveSteps(short step, bool bBlocking)
{
    m_NeedsUpdate = !bBlocking;
    bBlocking ? m_Stepper.move(step) : m_Stepper.startMove(step);
}

void ArduinoStepper::MoveDist(float distance, bool bBlocking)
{
    m_NeedsUpdate = !bBlocking;
    MoveSteps( (distance * m_StepsPerDistance), bBlocking);
}

short ArduinoStepper::GetStepsPerRevolution() const
{
    return m_Stepper.getSteps();
}

short ArduinoStepper::GetStepsPerDistance() const
{
    return m_StepsPerDistance;
}

void ArduinoStepper::SetStepsPerDistance(short spd)
{
    m_StepsPerDistance = spd;
}

void ArduinoStepper::SetMicroStep(EMicroStep ms)
{
    m_Stepper.setMicrostep(static_cast<short>(ms));
}

long ArduinoStepper::GetAccumulatedStep() const
{
    return m_Stepper.getStepsCompleted();
}

void ArduinoStepper::Rotate(float deg, bool bBlocking)
{
    m_NeedsUpdate = !bBlocking;
    bBlocking ? m_Stepper.rotate(deg) : m_Stepper.startRotate(deg);
}

void ArduinoStepper::BrakeStop()
{
    m_Stepper.startBrake();
}

void ArduinoStepper::ImmediateStop()
{
    m_Stepper.stop();
}

void ArduinoStepper::SetRPM(float rpm)
{
    m_Stepper.setRPM(rpm);
}

float ArduinoStepper::GetRPM() const
{
    return m_Stepper.getRPM();
}

bool ArduinoStepper::IsValid() const
{
    return true;
}

void ArduinoStepper::SetEnabled(bool bEnabled)
{
    (bEnabled ? m_Stepper.enable() : m_Stepper.disable());
}
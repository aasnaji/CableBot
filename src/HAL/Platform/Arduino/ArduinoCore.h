#pragma once
#ifndef ARDUINO_CORE_H
#define ARDUINO_CORE_H

#include "../../RobotCore.h"

#include <Arduino.h>

// Add more slots if your hardware support
DECL_ENUM(EMotorSlots, SLOT1, SLOT2, SLOT3, SLOT4);
DECL_ENUM(ESensorSlots, SLOT1, SLOT2, SLOT3, SLOT4, SLOT5, SLOT6);

class ArduinoCore : public RobotCore
{
public:
    ArduinoCore();

    virtual void Init() override;
    virtual void Shutdown() override;
    virtual void Update(float deltaTime) override;

    virtual void Log(const char* msg) override;

    virtual float GetTime() const override;
    virtual unsigned long GetTimeMilli() const override;
    virtual unsigned long GetTimeMicro() const override;

    // Lower level access methods. These will be wrapped by subclasses for semantic and syntactic nice-ness
    virtual Motor::IMotor* GetMotor(unsigned char id) const override;
    virtual Sensor::ISensor* GetSensor(unsigned char id) const override;

protected:
    void DetachMotor(EMotorSlots::Type slot);
    void DetachSensor(ESensorSlots::Type slot);

    virtual bool AttachMotorInternal(Motor::IMotor* motor) override;
    virtual bool AttachSensorInternal(Sensor::ISensor* sensor) override;
    
    virtual void LoadSchematic(const FSchematic& schematic) override {}

protected:
    Motor::IMotor* m_Motors[EMotorSlots::MAX_VAL];
    Sensor::ISensor* m_Sensors[ESensorSlots::MAX_VAL];
};
#endif // !ARDUINO_CORE_H

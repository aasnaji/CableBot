#pragma once
#ifndef ROBOT_INSTANCE_H
#define ROBOT_INSTANCE_H
#include "RobotCore.h"
#include "Schematic.h"

/*  Robot Instance
*   Describes a live instance of a robot object or application. This acts as the entry point for the
*   application, and is to be sub-classed to perform client specific implementations. There can only
*   be one RobotInstance - it can be instantiated by a factory method and initialized on the spot. Alternatively,
*   the instance can be constructed and initialized through a schematic. This automatically loads hardware info
*   automatically upon construction without client side initialization code.
*/
class RobotInstance
{
public:
    virtual ~RobotInstance();

    virtual void Init();
    virtual void Update();
    virtual void Shutdown();

    inline RobotCore& GetRobotCore() const {return *RC;}
    static inline RobotInstance* GetInstance() {return s_Instance;}
protected:
    RobotInstance();

    /* This is called by callback mechanisms to propogate events throughout the application */
    //virtual void OnEvent(Event& e);

protected:
    RobotCore* RC;
private:
    static RobotInstance* s_Instance;
    unsigned long m_prevMillis;
};
#endif // !ROBOT_INSTANCE_H
#pragma once
#ifndef HAL_FWD_H
#define HAL_FWD_H

#include "RobotInstance.h"
#include "Motors/MotorsFwd.h"
#include "Sensors/SensorsFwd.h"
#include "Schematic.h"

#endif // !HAL_FWD_H

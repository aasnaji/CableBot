#include "RobotInstance.h"

RobotInstance* RobotInstance::s_Instance = nullptr;

RobotInstance::RobotInstance()
{
    s_Instance = this;

    RC = RobotCore::Create();
}

RobotInstance::~RobotInstance()
{
    if(RC)
    {
        delete RC;
        RC = nullptr;
    }
}

void RobotInstance::Init()
{
    RC->Init();
}

void RobotInstance::Update()
{
    RC->Log("RobotInstance Update");

    const unsigned long currTime = RC->GetTimeMilli();
    const float deltaTime = (currTime - m_prevMillis) / 1000.f;
    m_prevMillis = currTime;

    RC->Update(deltaTime);
}

void RobotInstance::Shutdown()
{
    RC->Shutdown();
}